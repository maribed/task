<?php


namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $price
 * @property string $start_at
 * @property string $end_at
 *
 * @property User $user
 *
 * Class Projects
 * @package app\models
 */
class Projects extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'integer'],
            [
                'user_id',
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
            ['name', 'string'],
            [['start_at', 'end_at'], 'safe']
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
