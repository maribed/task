<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $login;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'trim'],
            ['login', 'required'],
            [
                'login',
                'match',
                'pattern' => '#^[\w_-]+$#i',
                'message' => 'В поле "Логин" возможны только латинские символы и цифры'
            ],
            [
                'login',
                'unique',
                'targetClass' => '\app\models\User',
            ],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['password', 'required'],
            [
                'password',
                'string',
                'min'     => 6,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login'      => 'Login',
            'password'   => 'Password',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->login = $this->login;
        $user->password = $this->password;

        return $user->save() ? $user : null;
    }
}
