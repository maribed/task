<?php
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-hover'
    ],
    'options' => [
        'class' => 'grid-view table-responsive'
    ],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'user.login',
        'name',
        'price',
        'start_at',
        'end_at',
    ]
]);
