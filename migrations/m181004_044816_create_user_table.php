<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181004_044816_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id'            => $this->primaryKey(),
            'login'         => $this->string()->notNull()->unique(),
            'auth_key'      => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
        ]);

        $this->createIndex('idx-user-login', '{{%user}}', 'login');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
