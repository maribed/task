<?php

use yii\db\Migration;

/**
 * Class m181004_054920_add_test_data
 */
class m181004_054920_add_test_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'login'         => 'admin',
            'auth_key'      => 'GE9wXI8WLqtuCeLWi4zToW6raXvjT-DB',
            'password_hash' => '$2y$13$vsOmmKhAyKXEPYrUtCZOhuUb54.PI00/.gkpafQRsLUMbWigpzI/2',
        ]);

        $this->insert('projects', [
            'user_id'  => '1',
            'name'     => 'Project 1',
            'price'    => '100',
            'start_at' => '2018-10-03 00:00:00',
            'end_at'   => '2018-10-06 00:00:00',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', ['login' => 'admin']);
        $this->delete('projects', ['name' => 'Project 1']);
    }
}
