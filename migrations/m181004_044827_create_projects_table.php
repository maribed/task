<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 */
class m181004_044827_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects', [
            'id'       => $this->primaryKey(),
            'user_id'  => $this->integer(10)->unsigned()->notNull(),
            'name'     => $this->string(100)->notNull(),
            'price'    => $this->money(10, 0)->notNull()->defaultValue(0),
            'start_at' => $this->dateTime()->notNull(),
            'end_at'   => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('projects');
    }
}
